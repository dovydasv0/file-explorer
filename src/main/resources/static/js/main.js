$(document).ready(function () {


});

function expandButtonClick(items) {
    var file = JSON.parse(items)
    if ($("#folder-" + file["name"]).hasClass("active")) {
        $("#folder-" + file["name"]).removeClass("active")
        $("#button-" + file["name"]).text("+");
        shrink(file["name"]);
    } else {
        var margin = $("#folder-" + file["name"]).css("margin-left");
        var marginValue = parseInt(margin.substring(0, margin.length - 2)) + 20
        $("#folder-" + file["name"]).addClass("active")
        $("#button-" + file["name"]).text("-");
        for (var i = file["folderItems"].length - 1; i >= 0; i--) {
            var insert;
            if (file["folderItems"][i]["type"] === "FOLDER") {
                insert = "<div style=\"margin-left: " + marginValue + "px\" class=\"folder-container item-" + file["name"] + "\" id=\"folder-" + file["folderItems"][i]['name'] + "\">\n" +
                    "                        <span>|- &#128193;" + file["folderItems"][i]['name'] + "</span>\n" +
                    "                        <button class=\"btn btn-outline-secondary btn-sm plus-button\" id=\"button-" + file["folderItems"][i]['name'] + "\" >+</button>\n" +
                    "                    </div>"
                $(insert).insertAfter("#folder-" + file["name"]);
                $("#button-" + file["folderItems"][i]['name']).click({param: JSON.stringify(file["folderItems"][i])}, function (event) {
                    expandButtonClick(event.data.param)
                })
            } else {
                var id ="file-button-" + file["folderItems"][i]['name']
                id = id.toLocaleLowerCase();
                id = id.split(".")[0];
                insert = "<button style=\"margin-left: " + marginValue + "px\" class=\"file-button item-" + file["name"] + "\" id=\""+id+"\">|- &#128441; " + file["folderItems"][i]['name'] + "</button>";
                $(insert).insertAfter("#folder-" + file["name"]);
                $("#"+id).click({param: file["folderItems"][i]["path"]}, function (event) {
                    readFile(event.data.param)
                })
            }


        }
    }
}

function readFile(path) {
    var data = encodeURIComponent(path)

    $.ajax({
        type: "POST",
        url: "/getFile",
        data: data,
        success: function(d){
            $("#text-box").val(d);
            $("#path-header").text(path)
        },
        error: function (e) {
            console.log(e)
        }
    })
}

function writeToFile() {
    var data = {
        path: encodeURIComponent($("#path-header").text()),
        text: $("#text-box").val()
    };
    console.log();
    $.ajax({
        type: "POST",
        url: "/saveFile",
        data: JSON.stringify(data),
        contentType: "application/json",
        success: function(d){
            alert("success")
        },
        error: function (e) {
            console.log(e)
        }
    })
}

function shrink(filename){
    var expandedFolders = $(".item-" + filename);
    for (var i = 0; i < expandedFolders.length; i++) {
        shrink(expandedFolders[i].id.substring(7))
    }
    expandedFolders.remove();

}