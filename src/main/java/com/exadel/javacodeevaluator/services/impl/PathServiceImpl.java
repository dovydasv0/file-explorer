package com.exadel.javacodeevaluator.services.impl;

import com.exadel.javacodeevaluator.models.FolderItem;
import com.exadel.javacodeevaluator.models.FolderItemType;
import com.exadel.javacodeevaluator.services.PathService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Service
public class PathServiceImpl implements PathService {
    @Value("${spring.application.rootPath}")
    private String rootPath;

    @Override
    public FolderItem getItemsInRootFolder() {
        System.out.println(rootPath);
        File rootFolder = new File(rootPath);
        return constructFileObject(rootFolder);
    }


    private FolderItem constructFileObject(File file){
        FolderItem result = new FolderItem(new ArrayList<>(), false, file.getName(), FolderItemType.FILE, file.getPath());
        if (file.isDirectory()){
            List<FolderItem> items = new ArrayList<>();
            result.setType(FolderItemType.FOLDER);
            File[] files = file.listFiles();
            if (files != null && files.length > 0){
                for (File file1 : files) {
                    items.add(constructFileObject(file1));
                }
            }
            result.setFolderItems(items);
        }
        return result;
    }
}
