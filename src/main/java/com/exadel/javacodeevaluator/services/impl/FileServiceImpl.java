package com.exadel.javacodeevaluator.services.impl;

import com.exadel.javacodeevaluator.payloads.SavePayload;
import com.exadel.javacodeevaluator.services.FileService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

@Service
public class FileServiceImpl implements FileService {



    @Override
    public String getFile(String path) {
        StringBuilder result = new StringBuilder();
        try (Stream<String> stream = Files.lines(Paths.get(path))) {

            stream.forEach( data -> {
                result.append(data);
                result.append("\n");
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.toString();
    }

    @Override
    public void writeToFile(SavePayload body) {
        try {
            FileWriter fileWriter = new FileWriter(body.getPath());
            fileWriter.write(body.getText());
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
