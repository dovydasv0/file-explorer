
package com.exadel.javacodeevaluator.services;

import com.exadel.javacodeevaluator.models.FolderItem;

public interface PathService {
    FolderItem getItemsInRootFolder();
}
