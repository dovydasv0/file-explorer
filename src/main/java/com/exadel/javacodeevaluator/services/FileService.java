package com.exadel.javacodeevaluator.services;

import com.exadel.javacodeevaluator.payloads.SavePayload;

public interface FileService {
    String getFile(String path);
    void writeToFile(SavePayload body);
}
