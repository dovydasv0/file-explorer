package com.exadel.javacodeevaluator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaCodeEvaluatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaCodeEvaluatorApplication.class, args);
    }

}

