package com.exadel.javacodeevaluator.models;

public enum FolderItemType {
    FOLDER,
    FILE
}
