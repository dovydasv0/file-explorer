package com.exadel.javacodeevaluator.models;

import java.util.Comparator;
import java.util.List;

public class FolderItem {
    private List<FolderItem> folderItems;
    private boolean isExpanded;
    private String name;
    private FolderItemType type;
    private String path;

    public FolderItem(List<FolderItem> folderItems, boolean isExpanded, String name, FolderItemType type, String path) {
        this.folderItems = folderItems;
        this.isExpanded = isExpanded;
        this.name = name;
        this.type = type;
        this.path = path;
    }

    public List<FolderItem> getFolderItems() {
        return folderItems;
    }

    public void setFolderItems(List<FolderItem> folderItems) {
        folderItems.sort(Comparator.comparing(FolderItem::getType));
        this.folderItems = folderItems;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FolderItemType getType() {
        return type;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("{\"name\":\"" + name + "\",\n" +
                "\"isExpanded\":" + isExpanded + ",\n" +
                "\"type\":\"" + type + "\",\n" +
                "\"path\":\"" + path.replace("\\","\\\\") + "\",\n" +
                "\"folderItems\":[");
        for (int i = 0; i < folderItems.size(); i++) {
            result.append(folderItems.get(i).toString());
            if (i<folderItems.size()-1){
                result.append(",");
            }
        }
        result.append("]}");


        return result.toString();
    }

    public void setType(FolderItemType type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
