
package com.exadel.javacodeevaluator.payloads;

public class SavePayload {
    private String path;
    private String text;

    public SavePayload() {
    }

    public SavePayload(String path, String text) {
        this.path = path;
        this.text = text;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
