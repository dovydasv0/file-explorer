package com.exadel.javacodeevaluator.controllers;

import com.exadel.javacodeevaluator.services.PathService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {

    private final PathService pathService;

    public MainController(PathService pathService) {
        this.pathService = pathService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String displayLogin(Model model) {
        model.addAttribute("files", pathService.getItemsInRootFolder());
        return "index";
    }
}
