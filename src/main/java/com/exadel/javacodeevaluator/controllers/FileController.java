package com.exadel.javacodeevaluator.controllers;

import com.exadel.javacodeevaluator.payloads.SavePayload;
import com.exadel.javacodeevaluator.services.FileService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import static java.net.URLDecoder.decode;

@RestController
public class FileController {

    private final FileService fileService;

    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    @PostMapping("/getFile")
    public ResponseEntity<?> getFile(@RequestBody String path){
        String result = null;
        try {
            result = decode(path, "UTF-8");
            result = fileService.getFile(result.substring(0, result.length()-1));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok(result);
    }

    @PostMapping("/saveFile")
    public ResponseEntity<?> saveFile(@RequestBody SavePayload body){
        body.setPath(decode(body.getPath()));
        fileService.writeToFile(body);
        return ResponseEntity.ok().build();
    }

}
