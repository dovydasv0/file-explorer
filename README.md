FILE EXPLORER

DISCLAIMER: This is the beta version you will encounter bugs

Requirements:
* Jdk 1.8 (min)
* Maven 3.5.4
* Git 2.19.2


Run guide
* clone git project to your local machine
```javascript
$ git clone git@bitbucket.org:dovydasv0/file-explorer.git
```
* Import maven project to your IDE
* Open CMD navigate to project directory
* Run mvn clean install
* Change application properties (src &rarr; main &rarr; resources &rarr; application.properties) "spring.application.rootPath" to your desired root path (default "C:/") 
```javascript
spring.application.rootPath=C:\\
```
* Run application (default port 9500)

you can access the application by going to: 

```javascript
http://localhost:9500/
```

Notes:
It is possible to specify the root path in run configuration as an argument. Go to "Edit Configuration..." (in dropdown menu near run button) &rarr; "Environment" &rarr; "Program arguments"
```javascript
--spring.application.rootPath=C:\\
```
This configuration will take priority over application properties configuration
